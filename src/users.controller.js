const users = require('./data/users.json');

const findAll = () => {
  return users;
};

const findOneById = id => {
  return users.find(user => user.id === parseInt(id));
};

module.exports = {
  findAll,
  findOneById
};